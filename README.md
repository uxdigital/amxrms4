Release Notes
=============

RMS SDK

FG #: 3004  
Current Version: 4.3.12
Release Date: 7/23/2014


Prerequisites
-------------
--  RMS Server version 4.3 or greater


Changes in this release
-----------------------
-- Reduced amount of disk space required at runtime
-- Added support for G5 touch panels
-- Added G5 touch panel file with new look and feel
-- Improved touch panel performance
-- Updated API to allow a default event booking duration to be specified
-- The keyboard is automatically displayed when creating a meeting on a G5 touch
   panel.  Updated API to allow this feature to be disabled
-- Updated RmsTouchPanelMonitor.axs to support G5 touch panels


Changes in 4.3.7
----------------
--  Added support for DriverDesign created .xdd modules
    (display devices only - Video Projector, Monitor, TV)
--  Added support for NetLinx NX series of controllers
--  Extended ASCII characters will now be handled correctly with the NetLinx APIs
--  Added support for the DVX-3156
--  Total attendee count will now contain the correct value in the
    RmsEventBookingResponse structure for scheduling callbacks
--  Corrected an issue with the ShowRmsEventBookingResponse function in the NetLinx
    Sample (Advanced Scheduling) program which could lead to incorrect data being displayed
--  Corrected an issue which resulted in end and start events being fired when a meeting
    was extended
--  Corrected an issue which prevented the RmsEventSchedulingEndResponse callback function
    from firing when ending a booking
--  Added the asset key to the response of the ?asset.location query
--  Corrected an issue with the audio mic array in the DVX switcher monitor
--  Corrected an issue which prevented display usage from working with certain Duet module


Known Issues
------------
--  No known issues.